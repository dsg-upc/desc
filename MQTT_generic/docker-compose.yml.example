version: '3.7'

services:

  loratrust-frontend:
    container_name: loratrust-frontend
    image: dkr-dsg.ac.upc.edu/loratrust/frontend:main__a36e46c
    ports:
      - 3000:80
    environment:
      - CHOKIDAR_USEPOLLING=true
      - API_URL=${API_URL:-http://localhost:8080}
      - SHARE_URL=${SHARE_URL:-http://localhost:4000/contracts}

  loratrust-persistence-timescaledb:
    restart: unless-stopped
    container_name: loratrust-persistence-timescaledb
    # src https://hub.docker.com/r/matrixdotorg/synapse/tags
    image: timescale/timescaledb:2.10.0-pg14
    environment:
      - POSTGRES_USER=${TIMESCALE_DB_USER}
      - POSTGRES_PASSWORD=${TIMESCALE_DB_PASSWD}
      - POSTGRES_INITDB_ARGS=--encoding=UTF-8 --lc-collate=C --lc-ctype=C
    volumes:
      - timescaledb:/var/lib/postgresql/data
    ports:
      - 5432:5432

  loratrust-backend:
    restart: unless-stopped
    container_name: loratrust-backend
    image: dkr-dsg.ac.upc.edu/loratrust/backend:main__994fe47
    ports:
      - 8080:8080
    environment:
      - ALLOWED_ORIGIN='*'
      - ALLOWED_HEADERS='Origin, X-Requested-With, Content-Type, Accept, Authorization'
      - BLOCKCHAIN_URL=${BLOCKCHAIN_URL:-http://localhost:8545}
      - SHARE_URL=${SHARE_URL:-http://loratrust-truffle-nginx:80/contracts}
      - DB_HOST=loratrust-persistence-timescaledb
      - TIMESCALE_URL=postgres://${TIMESCALE_DB_USER}:${TIMESCALE_DB_PASSWD}@loratrust-persistence-timescaledb:5432/timescaledb

    init: true
    depends_on:
      - loratrust-persistence-timescaledb

  loratrust-truffle:
    container_name: loratrust-truffle
    image: dkr-dsg.ac.upc.edu/loratrust/truffle:main__6a65a78
    environment:
      - BC_HOST=loratrust-blockchain
      - BC_PORT=8545
    volumes:
      - share:/app/build:rw
      - truffle:/app:rw
    # why this container takes more time to stop if it's just a sleep infinity? -> src https://vsupalov.com/docker-compose-stop-slow/
    #   dash is difficult to trap -> src https://unix.stackexchange.com/questions/314554/why-do-i-get-an-error-message-when-trying-to-trap-a-sigint-signal
    #     https://unix.stackexchange.com/questions/240723/exit-trap-in-dash-vs-ksh-and-bash
    # we can use init https://stackoverflow.com/questions/50356032/whats-the-docker-compose-equivalent-of-docker-run-init
    init: true

  loratrust-truffle-nginx:
    container_name: loratrust-truffle-nginx
    image: dkr-dsg.ac.upc.edu/loratrust/truffle-nginx:main__5775cac
    ports:
      - 4000:80
    volumes:
      - share:/usr/share/nginx/html:ro

  loratrust-blockchain:
    container_name: loratrust-blockchain
    image: trufflesuite/ganache:v7.5.0
    ports:
      - 8545:8545
    # allow pipe in entrypoint -> src https://stackoverflow.com/questions/30441178/how-to-use-pipesioredirection-in-dockerfile-run/30441252#30441252
    entrypoint: "sh -c 'node /app/dist/node/cli.js ${GANACHE_OPTS:-} | tee /share/contracts/ganache.log'"
    # because of the sh cmd in entrypoint
    init: true
    volumes:
      - blockchain:/ganache:rw
      - share:/share:rw

  loratrust-devicesim:
    restart: unless-stopped
    container_name: loratrust-devicesim
    # TODO change name
    image: dkr-dsg.ac.upc.edu/loratrust/devicesim:main__df64f6f
    init: true
    environment:
      - API_URL=${API_URL:-http://loratrust-backend:8080}
      - MEASUREMENT_INTERVAL=50
      - EVENT_INTERVAL=50
      - METADATA_INTERVAL=50
    depends_on:
      - loratrust-backend

  loratrust-auditor:
    restart: unless-stopped
    container_name: demo2_loratrust-auditor
    image: dkr-dsg.ac.upc.edu/loratrust/auditor:main__93cf984
    init: true
    # TODO
    environment:
      - POLL_INTERVAL=10
    depends_on:
      - loratrust-blockchain

  loratrust-emqx:
    container_name: demo2_loratrust-mqtt-broker
    image: emqx/emqx:5.1.0
    environment:
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__BODY=\"$${payload}\""
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__CONNECT_TIMEOUT=15s"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__ENABLE_PIPELINING=100"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__HEADERS={\"content-type\" = \"application/json\"}"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__METHOD=post"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__POOL_SIZE=8"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__POOL_TYPE=random"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__RESOURCE_OPTS__AUTO_RESTART_INTERVAL=60s"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__RESOURCE_OPTS__HEALTH_CHECK_INTERVAL=15s"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__RESOURCE_OPTS__INFLIGHT_WINDOW=100"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__RESOURCE_OPTS__MAX_BUFFER_BYTES=1GB"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__RESOURCE_OPTS__QUERY_MODE=async"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__RESOURCE_OPTS__REQUEST_TIMEOUT=15s"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__RESOURCE_OPTS__WORKER_POOL_SIZE=4"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__SSL__ENABLE=false"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__SSL__VERIFY=verify_peer"
      - "EMQX_BRIDGES__WEBHOOK__BACKEND__URL=http://demo2.loratrust.com:18080/save_data"
      - "EMQX_RULE_ENGINE__IGNORE_SYS_MESSAGE=true"
      - "EMQX_RULE_ENGINE__JQ_FUNCTION_DEFAULT_TIMEOUT=10s"
      - "EMQX_RULE_ENGINE__RULES__RULE_4XSC__ACTIONS__1=webhook:backend"
      - "EMQX_RULE_ENGINE__RULES__RULE_4XSC__DESCRIPTION=ingest mqtt topic to backend"
      - "EMQX_RULE_ENGINE__RULES__RULE_4XSC__METADATA={\"created_at\" = 1688553193098}"
      - "EMQX_RULE_ENGINE__RULES__RULE_4XSC__SQL=\"SELECT payload FROM \\\"to-server/#\\\"\""
    ports:
      - 1883:1883
      - 127.0.0.1:8883:8883
      - 127.0.0.1:8084:8084
      - 127.0.0.1:18083:18083


volumes:
  blockchain:
  # loratrust-truffle compiles smartcontracts and truffle-nginx serve it to
  #   loratrust-frontend via http through share docker volume
  #   src -> https://www.baeldung.com/ops/docker-share-volume-multiple-containers
  share:
  truffle:
  matrix_data:
  matrix_db_data:
  timescaledb:
